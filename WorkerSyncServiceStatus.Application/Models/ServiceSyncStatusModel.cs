﻿using System;

namespace WorkerSyncServiceStatus.Application.Models
{
    public class ServiceSyncStatusModel
    {
        public int Id { get; set; }
        public Guid TenantId { get; set; }
        public DateTime DataCriacao { get; set; }
        public int CanalCriacaoId { get; set; }
        public string User_Id { get; set; }
        public int? Cliente_Id { get; set; }
        public string ErrorMessage { get; set; }
        public string FullErrorMessage { get; set; }
        public int CodigoConfiguracao { get; internal set; }
        public int? TotalErros { get; set; }
        public int IdStatus { get; set; }
        public bool Running { get; set; }
    }
}
