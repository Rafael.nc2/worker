﻿using System.Threading.Tasks;

namespace WorkerSyncServiceStatus.Application.Interface
{
    public interface IRegisterSyncStatusService
    {
        void GetFromQueue();
    }
}
