﻿using System;
using WorkerSyncServiceStatus.Application.Models;
using WorkerSyncServiceStatus.Domain.Entities;

namespace WorkerSyncServiceStatus.Application.Mappings
{
    public static class SituacaoServicoEntitieMapping
    {
        public static SituacaoServico Convert(ServiceSyncStatusModel serviceSyncStatus)
        {
            return new SituacaoServico { 
                Id = serviceSyncStatus.Id,
                CanalId = serviceSyncStatus.CanalCriacaoId,
                Erro = serviceSyncStatus.FullErrorMessage,
                DataCriacao = serviceSyncStatus.DataCriacao,
                DataUltimaAtualizacao = DateTime.Now,
                TotalErros = serviceSyncStatus.TotalErros ?? 0,
                DtUltAtu = (serviceSyncStatus.DataCriacao == null ? DateTime.Now : serviceSyncStatus.DataCriacao),
                CodigoConfiguracao = serviceSyncStatus.CodigoConfiguracao,
                UserId = serviceSyncStatus.User_Id,
                IdStatus = serviceSyncStatus.IdStatus,
                TenantId = serviceSyncStatus.TenantId,
                Running = serviceSyncStatus.Running
            };
        }
    }
}
