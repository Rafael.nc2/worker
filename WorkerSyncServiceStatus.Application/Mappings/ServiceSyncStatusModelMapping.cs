﻿using WorkerSyncServiceStatus.Application.Models;
using WorkerSyncServiceStatus.Domain.Entities;

namespace WorkerSyncServiceStatus.Application.Mappings
{
    public class ServiceSyncStatusModelMapping
    {
        public ServiceSyncStatusModel Convert(SituacaoServico situacaoServico) => 
            new ServiceSyncStatusModel { 
                Id = situacaoServico.Id,
                TenantId = situacaoServico.TenantId,
                DataCriacao = situacaoServico.DataCriacao,
                CanalCriacaoId = situacaoServico.CanalId,
                User_Id = situacaoServico.UserId,
                Cliente_Id = 0,
                ErrorMessage = situacaoServico.Erro,
                FullErrorMessage = situacaoServico.Erro,
                CodigoConfiguracao = situacaoServico.CodigoConfiguracao,
                TotalErros = situacaoServico.TotalErros,
                IdStatus = situacaoServico.IdStatus,
                Running = situacaoServico.Running
            };
    }
}
