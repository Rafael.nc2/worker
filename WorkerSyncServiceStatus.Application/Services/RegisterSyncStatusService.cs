﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using WorkerSyncServiceStatus.Application.Interface;
using WorkerSyncServiceStatus.Application.Mappings;
using WorkerSyncServiceStatus.Application.Models;
using WorkerSyncServiceStatus.Application.Repositories;
using WorkerSyncServiceStatus.Domain.Entities;

namespace WorkerSyncServiceStatus.Application.Services
{
    public class RegisterSyncStatusService : IRegisterSyncStatusService
    {
        private readonly ISituacaoServicoRepository _situacaoServicoRepository;

        private IEnumerable<SituacaoServico> serviceList;

        public RegisterSyncStatusService(ISituacaoServicoRepository situacaoServicoRepository)
        {
            _situacaoServicoRepository = situacaoServicoRepository;
        }

        const string QUEUE_URL = "amqp://udlsysah:nllLVE-VInmdwJGIBxvjniPFDQ9ysrQE@fly.rmq.cloudamqp.com/udlsysah";
        const string QUEUE_NAME = "syncStatusService";

        private IConnection _connection;
        private IModel _channel;

        public void GetFromQueue()
        {
            QueueInit();

            var consumer = new EventingBasicConsumer(_channel);

            consumer.Received += async (model, ea) =>
            {
                var body = ea.Body.ToArray();
                var message = Encoding.UTF8.GetString(body);
                await SaveContent(message);
            };

            _channel.BasicConsume(
                queue: QUEUE_NAME,
                autoAck: true,
                consumer: consumer
            );
        }

        private void QueueInit()
        {
            ConnectionFactory factory = new ConnectionFactory
            {
                Uri = new Uri(QUEUE_URL)
            };

            _connection = factory.CreateConnection();
            _channel = _connection.CreateModel();

            _channel.QueueDeclare(
                queue: QUEUE_NAME,
                durable: false,
                exclusive: false,
                autoDelete: false,
                arguments: null
            );
        }

        private async Task GetAllServices(List<ServiceSyncStatusModel> services)
        {
            serviceList = await _situacaoServicoRepository.GetAllServicesFromUserId(services[0].User_Id);
        }

        private async Task SaveContent(string message)
        {
            if (!String.IsNullOrEmpty(message))
            {
                var servicesFromRequest = JsonSerializer.Deserialize<List<ServiceSyncStatusModel>>(message);
                await GetAllServices(servicesFromRequest);

                foreach (var item in servicesFromRequest)
                {
                    var service = serviceList.Where(service => service.CanalId == item.CanalCriacaoId);
                    if (service.Any()) item.Id = service.First().Id;
                    
                    var situacaoServico = SituacaoServicoEntitieMapping.Convert(item);

                    _situacaoServicoRepository.Save(situacaoServico);
                }

                Console.WriteLine($"{DateTime.Now} - {message}");
            }
        }

    }
}
