﻿using System.Collections.Generic;
using System.Threading.Tasks;
using WorkerSyncServiceStatus.Domain.Entities;

namespace WorkerSyncServiceStatus.Application.Repositories
{
    public interface ISituacaoServicoRepository
    {
        Task<IEnumerable<SituacaoServico>> GetAllServicesFromUserId(string userId);
        Task<SituacaoServico> GetServiceById(string tenantId, int serviceId);

        void Save(SituacaoServico situacaoServico);
    }
}
