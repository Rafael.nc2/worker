﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WorkerSyncServiceStatus.Domain.Entities
{
    [Table("SituacaoServicoDev")]
    public class SituacaoServico
    {
        [Key]
        public int Id { get; set; }
        public int CanalId { get; set; }
        public string Erro { get; set; }
        public DateTime DataCriacao { get; set; }
        public DateTime? DataUltimaAtualizacao { get; set; }
        public int TotalErros { get; set; }
        public DateTime DtUltAtu { get; set; }
        public int CodigoConfiguracao { get; set; }
        public string UserId { get; set; }
        public int IdStatus { get; set; }
        public Guid TenantId { get; set; }
        public bool Running { get; set; }
    }
}
