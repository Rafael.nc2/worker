﻿using System;
using System.Data;
using System.Data.SqlClient;
using WorkerSyncServiceStatus.Shared.Helpers;

namespace WorkerSyncServiceStatus.Infrasctructure.DataAccess.Dapper.Context
{
    public class DapperContext : IDisposable
    {

        public SqlConnection Connection { get; set; }

        public DapperContext()
        {
            Connection = new SqlConnection(ConnectionStringHelper.DefaultConnectionString);
            Connection.Open();
        }

        public void Dispose()
        {
            if (Connection.State != ConnectionState.Closed)
            {
                Connection.Close();
            }
        }
    }
}
