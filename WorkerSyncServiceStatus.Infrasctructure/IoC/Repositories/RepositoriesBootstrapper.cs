﻿using Microsoft.Extensions.DependencyInjection;
using WorkerSyncServiceStatus.Application.Repositories;
using WorkerSyncServiceStatus.Infrasctructure.DataAccess.Dapper.Context;
using WorkerSyncServiceStatus.Infrasctructure.Repositories;

namespace WorkerSyncServiceStatus.Infrasctructure.IoC.Repositories
{
    public class RepositoriesBootstrapper
    {
        internal void ChildServiceRegister(IServiceCollection services)
        {
            services.AddScoped<DapperContext, DapperContext>();
            services.AddScoped<ISituacaoServicoRepository, SituacaoServicoRepository>();
        }
    }
}
