﻿using Microsoft.Extensions.DependencyInjection;
using WorkerSyncServiceStatus.Application.Interface;
using WorkerSyncServiceStatus.Application.Services;

namespace WorkerSyncServiceStatus.Infrasctructure.IoC.Application
{
    internal class ApplicationBootstrapper
    {
        internal void ChildServiceRegister(IServiceCollection services)
        {
            services.AddScoped<IRegisterSyncStatusService, RegisterSyncStatusService>();
        }
    }
}
