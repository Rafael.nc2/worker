﻿using Microsoft.Extensions.DependencyInjection;
using WorkerSyncServiceStatus.Infrasctructure.IoC.Application;
using WorkerSyncServiceStatus.Infrasctructure.IoC.Repositories;

namespace WorkerSyncServiceStatus.Infrasctructure.IoC
{
    public class RootBootstrapper
    {
        public void BootstrapperRegiterServices(IServiceCollection services)
        {
            //Application Services
            new ApplicationBootstrapper().ChildServiceRegister(services);

            //Repositories Services
            new RepositoriesBootstrapper().ChildServiceRegister(services);
        }
    }
}
