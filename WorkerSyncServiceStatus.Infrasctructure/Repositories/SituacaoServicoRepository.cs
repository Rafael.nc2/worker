﻿using Dapper;
using Dapper.Contrib.Extensions;
using System.Collections.Generic;
using System.Threading.Tasks;
using WorkerSyncServiceStatus.Application.Repositories;
using WorkerSyncServiceStatus.Domain.Entities;
using WorkerSyncServiceStatus.Infrasctructure.DataAccess.Dapper.Context;

namespace WorkerSyncServiceStatus.Infrasctructure.Repositories
{
    public class SituacaoServicoRepository : ISituacaoServicoRepository
    {
        private readonly DapperContext _context;

        public SituacaoServicoRepository(DapperContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<SituacaoServico>> GetAllServicesFromUserId(string userId)
        {
            string sql = @"SELECT	Id,
                                    CanalId,
                                    Erro,
                                    DataCriacao,
                                    DataUltimaAtualizacao,
                                    TotalErros,
                                    DtUltAtu,
                                    CodigoConfiguracao,
                                    UserId,
                                    IdStatus,
                                    TenantId,
                                    Running
                            FROM	SituacaoServicoDev as ss (NOLOCK)
                            WHERE	UserId = @userId";

            return await _context
                .Connection
                .QueryAsync<SituacaoServico>(
                    sql, 
                    new { userId }
                );
        }

        public Task<SituacaoServico> GetServiceById(string tenantId, int serviceId)
        {
            throw new System.NotImplementedException();
        }

        public void Save(SituacaoServico situacaoServico)
        {
            if(situacaoServico.Id > 0)
            {
                _context
                    .Connection
                    .Update(situacaoServico);
            }
            else
            {
                _context
                    .Connection
                    .Insert(situacaoServico);
            }
        }
    }
}
