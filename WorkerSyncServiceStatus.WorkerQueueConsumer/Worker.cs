using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;
using WorkerSyncServiceStatus.Application.Interface;

namespace WorkerSyncServiceStatus.WorkerQueueConsumer
{
    public class Worker : BackgroundService
    {
        private IServiceProvider _services;
        private readonly ILogger<Worker> _logger;

        public Worker(IServiceProvider services, ILogger<Worker> logger)
        {
            _services = services;
            _logger = logger;
        }

        //https://docs.microsoft.com/en-us/aspnet/core/fundamentals/host/hosted-services?view=aspnetcore-2.2&tabs=visual-studio#consuming-a-scoped-service-in-a-background-task
        private void Start()
        {
            _logger.LogInformation("###################################Consume Scoped Service Hosted Service is working.############################");

            var scope = _services.CreateScope();
            
            var scopedProcessingService = scope.ServiceProvider.GetRequiredService<IRegisterSyncStatusService>();

            scopedProcessingService.GetFromQueue();
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation("#################################Consume Scoped Service Hosted Service is starting.#######################################");

            Start();
        }
    }
}
